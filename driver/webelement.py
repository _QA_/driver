from selenium.webdriver.remote.webelement import WebElement as _WebElement
from selenium.webdriver.support.select import Select

from .mixins import SelectorMixin, WaitMixin


class WebElement(SelectorMixin, WaitMixin, _WebElement):

    @property
    def tag(self):
        return self.tag_name.lower()

    @property
    def type(self):
        return self.get_attribute('type')

    @property
    def value(self):
        return self.get_attribute('value')

    # TODO: examine this
    # @property
    # def id(self):
    #     return self.get_attribute('id')

    # ***************
    # *   STATES    *
    # ***************
    # Builtin:      #
    # -is_displayed #
    # -is_enabled   #
    # -is_selected  #

    def is_ready(self):
        return bool(self.is_displayed() and self.is_enabled())

    # ****************
    # *   WAITING    *
    # ****************

    def wait_until_visible(self, timeout=None):
        return self.wait_until(
            function=lambda element: element.is_displayed(),
            timeout=timeout
        )

    def wait_until_not_visible(self, timeout=None):
        return self.wait_until_not(
            function=lambda elem: elem.is_displayed(),
            timeout=timeout
        )

    def wait_until_ready(self, timeout=None):
        return self.wait_until(
            function=lambda element: element.is_ready(),
            timeout=timeout
        )

    def wait_until_not_ready(self, timeout=None):
        return self.wait_until_not(
            function=lambda elem: elem.is_ready(),
            timeout=timeout
        )

    # ***********************
    # *   INPUT HANDLING    *
    # ***********************

    def write(self, value):
        self.wait_until_ready()
        # If value is bytes convert to string
        if isinstance(value, bytes):
            value = value.decode('unicode')
        self.send_keys(value)

    def _checkbox(self, value):
        if (value is True and not self.is_selected()) or (value is False and self.is_selected()):
            self.wait_until_ready()
            self.click()

    def check(self):
        self._checkbox(value=True)

    def uncheck(self):
        self._checkbox(value=False)

    def _select(self, positive, selector):
        # TODO: check if is_selected can give info about state and prevent action if it's unnecessary
        try:
            method, value = selector.split('=', 1)
        except ValueError:
            raise ValueError('Invalid "select" selector.')

        prefix = '' if positive is True else 'de'
        method = 'visible_text' if method == 'text' else method
        value = int(value) if value == 'index' else value
        self.wait_until_ready()
        return getattr(Select(webelement=self), f"{prefix}select_by_{method}")(value)

    def select(self, selector):
        self._select(positive=True, selector=selector)

    def deselect(self, selector):
        self._select(positive=False, selector=selector)
