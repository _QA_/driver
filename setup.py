#!/usr/bin/env python
from distutils.core import setup

from setuptools import find_packages


setup(**{
    'name': 'driver',
    'version': '0.0.5',
    'author': 'Wojciech Zelek',
    'author_email': 'zelo@zelo.pl',
    'packages': find_packages(include='driver.*'),
    'install_requires': [
        'selenium>=3,<4'
    ],
})
