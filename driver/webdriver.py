from .webelement import WebElement

from .mixins import SelectorMixin, WaitMixin


class WebDriverMixin(SelectorMixin, WaitMixin):
    _web_element_cls = WebElement

    # ***********
    # * WAITING *
    # ***********

    def wait_until_page_loaded(self, timeout=None):
        return self.wait_until(
            function=lambda driver: driver.execute_script('return document.readyState') == 'complete',
            timeout=timeout or self.default_wait_timeout
        )
