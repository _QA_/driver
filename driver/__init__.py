import selenium.webdriver

from .webdriver import WebDriverMixin


class BlackBerry(WebDriverMixin, selenium.webdriver.BlackBerry):
    pass


class Chrome(WebDriverMixin, selenium.webdriver.Chrome):
    pass


class Edge(WebDriverMixin, selenium.webdriver.Edge):
    pass


class Firefox(WebDriverMixin, selenium.webdriver.Firefox):
    pass


class Ie(WebDriverMixin, selenium.webdriver.Ie):
    pass


class Opera(WebDriverMixin, selenium.webdriver.Opera):
    pass


class Remote(WebDriverMixin, selenium.webdriver.Remote):
    pass


class Safari(WebDriverMixin, selenium.webdriver.Safari):
    pass


class WebkitGTK(WebDriverMixin, selenium.webdriver.WebKitGTK):
    pass


__all__ = ('BlackBerry', 'Chrome', 'Edge', 'Firefox', 'Ie', 'Opera', 'Remote', 'Safari', 'WebkitGTK')
