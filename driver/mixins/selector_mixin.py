from selenium.webdriver.common.by import By

SELECTORS = {
    'id': lambda expression: (By.ID, expression),
    'xpath': lambda expression: (By.XPATH, expression),
    'name': lambda expression: (By.NAME, expression),
    'link': lambda expression: (By.LINK_TEXT, expression),
    'css': lambda expression: (By.CSS_SELECTOR, expression),
    'class': lambda expression: (By.CLASS_NAME, expression),
    'tag': lambda expression: (By.TAG_NAME, expression)
}


class SelectorMixin:
    _selectors = dict(SELECTORS)

    def add_selector(self, method, function):
        self._selectors[method] = function

    def _parse_selector(self, selector):
        try:
            method, expression = selector.split('=', 1)
        except ValueError:
            raise ValueError('Invalid selector format for selector: "{}"'.format(selector))
        try:
            method, expression = self._selectors[method](expression=expression)
        except KeyError:
            raise ValueError('Invalid selector method for method: "{}""'.format(method))

        return method, expression

    def find_element_by(self, selector):
        method, expression = self._parse_selector(selector=selector)
        return self.find_element(by=method, value=expression)

    def find_elements_by(self, selector):
        method, expression = self._parse_selector(selector=selector)
        return self.find_elements(by=method, value=expression)
