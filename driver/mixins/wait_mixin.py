from selenium.webdriver.support.wait import WebDriverWait


class WaitMixin:
    # TODO: handle timeout configuration in better way
    default_wait_timeout = 10

    def wait_until(self, function, message='', **kwargs):
        kwargs['timeout'] = kwargs.get('timeout') or self.default_wait_timeout
        return WebDriverWait(driver=self, **kwargs).until(function, message)

    def wait_until_not(self, function, message='', **kwargs):
        kwargs['timeout'] = kwargs.get('timeout') or self.default_wait_timeout
        return WebDriverWait(driver=self, **kwargs).until(function, message)

    def wait_until_element_present(self, selector, timeout=None):
        return self.wait_until(
            function=lambda context: context.find_element_by(selector=selector).is_displayed(),
            timeout=timeout or self.default_wait_timeout
        )

    def wait_until_element_not_present(self, selector, timeout=None):
        return self.wait_until_not(
            function=lambda context: context.find_element_by(selector=selector).is_displayed(),
            timeout=timeout or self.default_wait_timeout
        )
